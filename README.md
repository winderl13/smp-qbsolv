# SMP/SMTI-qbsolv
qbsolv based implementation for SMP/SMTI. With aproximation algorithms

## Requirements: 
````
numpy
dwave-qbsolv
ortools
matplotlib
dwave-system
````  
## Setup:  

This is a project using pipenv, please install packages using:

```
pipenv install
```
Run the programm then via: 
```
pipenv run python main.py
```
## Tests

All unit tests can be found in `./tests`

## Further details:

The implementation can be found in this folder.
The datasets can either be generated or the `instances.zip` can be used. 
Just unzip and put the content in a folder `./storage`.

The results from the evaluation can be found in `results.zip`. Also put them
in the `./storage` folder.