import statistics

from algorithms import storage
import matplotlib.pyplot as plt
import numpy as np


def add_correct_solution(all_s, correct_s, correct_solutions):
    if all_s == 0:
        correct_solutions.append(float(0))
    else:
        correct_solutions.append(correct_s / all_s)


def main():
    plt.clf()
    plot_sizes_qubo_vs_lp()
    plt.clf()
    plot_sizes_vs_accuracy()
    plt.clf()
    compute_accuracity()


def plot_sizes_qubo_vs_lp():
    """
    comparison qubo_energy vs lp energy
    results get cached due a longer computation

    :return:
    """
    sizes = [5, 10, 15, 20, 25, 30]
    p1s = [0.1, 0.5, 0.9]
    p2s = [0.1, 0.5, 0.9]
    num = 50
    num_rep = 100
    all_size_data = []
    stored = True
    if not stored:
        for size in sizes:
            all_size = 0
            data = [0, 0, 0]
            for p1 in p1s:
                for p2 in p2s:
                    for index_f in range(num):
                        for rep in range(6):
                            qubo_res = storage.get_smti_accuracity_measurement(size, p1, p2, num_rep, index_f, num, rep)
                            if "none" in qubo_res.keys():
                                continue  # skipping 1 qubos
                            else:
                                lp_out = qubo_res["lp_out"]["stability"]
                                lp_en = qubo_res["lp_out"]["en"]
                                qubo_out = qubo_res["min"]["stability"]
                                qubo_en = qubo_res["min"]["energy"]
                                all_size += 1
                                if not lp_out == qubo_out:
                                    if lp_en < qubo_en:
                                        data[1] += 1
                                    else:
                                        data[0] += 1
                                else:
                                    data[2] += 1
            all_size_data.append(list(map(lambda x: float(x) / all_size, data)))

        false_lp_en = np.array(list(map(lambda x: x[0], all_size_data)))
        false_qbsolv_en = np.array(list(map(lambda x: x[1], all_size_data)))
        correct_solution = np.array(list(map(lambda x: x[2], all_size_data)))
        storage.store_object(false_lp_en, "false_lp_en.csv", type="LIST")
        storage.store_object(false_qbsolv_en, "false_qbsolv_en.csv", type="LIST")
        storage.store_object(correct_solution, "correct_solution.csv", type="LIST")
        false_lp_en = np.array(list(map(lambda x: 100 * x[0], all_size_data)))
        false_qbsolv_en = np.array(list(map(lambda x: 100 * x[1], all_size_data)))
        correct_solution = np.array(list(map(lambda x: 100 * x[2], all_size_data)))
    else:
        false_lp_en = list(map(lambda x: 100 * float(x), storage.get_object("false_lp_en.csv", type="LIST")[0]))
        false_qbsolv_en = list(map(lambda x: 100 * float(x), storage.get_object("false_qbsolv_en.csv", type="LIST")[0]))
        correct_solution = list(
            map(lambda x: 100 * float(x), storage.get_object("correct_solution.csv", type="LIST")[0]))

    width = 3
    fig, ax = plt.subplots()
    ax.bar(sizes, false_lp_en, width, label="qbsolv en is better")
    ax.bar(sizes, correct_solution, width, label="correct solution")
    ax.bar(sizes, false_qbsolv_en, width, bottom=correct_solution, label="lp en is better")
    plt.yticks([i * 10 for i in (range(1, 11, 3))])
    lgd = plt.legend(loc='lower center', ncol=3, bbox_to_anchor=(0.5, -0.22))
    plt.xlabel("problem size")
    plt.ylabel("portion of a instance [%]")
    plt.savefig("./figures/energy_qubo_comparsion.png", bbox_extra_artists=(lgd,), bbox_inches='tight')


def compute_accuracity():
    """
    QUBO accuracy sizes: [5, 8, 11, 14, 17, 20]

    :return:
    """
    sizes = [5, 8, 11, 14, 17, 20]
    p1s = [0.1, 0.5, 0.9]
    p2s = [0.1, 0.5, 0.9]
    mean_qbsolv = []
    mean_lp = []
    mean_shiftbrk = []
    mean_kirialy = []
    for size in sizes:
        mean_qbsolv_1 = []
        mean_lp_1 = []
        mean_shiftbrk_1 = []
        mean_kirialy_1 = []
        for p1 in p1s:
            for p2 in p2s:
                res_all = storage.get_smti_computational_time(size, p1, p2)
                res_size = list(map(lambda x: [x[i] for i in range(len(x)) if i % 3 == 1], res_all[1:]))
                res_0 = list(map(lambda x: float(x[1]) / float(x[0]), filter(lambda x: not float(x[0]) == 0, res_size)))
                res_1 = list(map(lambda x: float(x[1]) / float(x[1]), filter(lambda x: not float(x[1]) == 0, res_size)))
                res_2 = list(map(lambda x: float(x[1]) / float(x[2]), filter(lambda x: not float(x[2]) == 0, res_size)))
                res_3 = list(map(lambda x: float(x[1]) / float(x[3]), filter(lambda x: not float(x[3]) == 0, res_size)))
                mean_qbsolv_1.append(statistics.mean(res_0))
                mean_lp_1.append(statistics.mean(res_1))
                mean_shiftbrk_1.append(statistics.mean(res_2))
                mean_kirialy_1.append(statistics.mean(res_3))

        mean_qbsolv.append(statistics.mean(mean_qbsolv_1))
        mean_lp.append(statistics.mean(mean_lp_1))
        mean_shiftbrk.append(statistics.mean(mean_shiftbrk_1))
        mean_kirialy.append(statistics.mean(mean_kirialy_1))

    plt.plot(sizes, mean_qbsolv, label="QUBO-MAX-SMTI")
    # plt.plot(sizes, mean_lp, label="MAX-SMTI-LP")
    plt.plot(sizes, mean_shiftbrk, label="SHIFTBRK")
    plt.plot(sizes, mean_kirialy, label="Krialy2")
    plt.xticks(sizes)
    plt.ylabel('accuracy factor')
    plt.xlabel('problem size')
    plt.legend()
    plt.savefig("./figures/qubo_accuracity_vs_apx.png")


def plot_sizes_vs_accuracy():
    """
    plot the problemsize vs the acc of qbsolv
    results got cached due a longer computation
    :return:
    """
    sizes = [5, 10, 15, 20, 25, 30]
    p1s = [0.1, 0.5, 0.9]
    p2s = [0.1, 0.5, 0.9]
    num_reps = [10, 100, 200]
    num = 50
    correct_solutions_10 = []
    correct_solutions_100 = []
    correct_solutions_200 = []
    computed = True
    if not computed:
        for size in sizes:
            for num_rep in num_reps:
                correct_s = 0
                all_s = 0
                for index_f in range(num):
                    for p1 in p1s:
                        for p2 in p2s:
                            for rep in range(6):
                                header = storage.get_smti_header(size, num, p1, p2, index_f)
                                pref_size = max(header["male"]["pref"]["mean"], header["female"]["pref"]["mean"])
                                qubo_res = storage.get_smti_accuracity_measurement(size, p1, p2, num_rep, index_f, num,
                                                                                   rep)
                                if "none" in qubo_res.keys():
                                    continue  # skipping 1 qubos
                                else:

                                    lp_out = qubo_res["lp_out"]["stability"]
                                    # lp_en = qubo_res["lp_out"]["en"]
                                    qubo_out = qubo_res["min"]["stability"]
                                    # qubo_en = qubo_res["min"]["en"]

                                    all_s += 1
                                    if qubo_out == lp_out:
                                        correct_s += 1
                if num_rep == 10:
                    add_correct_solution(all_s, correct_s, correct_solutions_10)
                elif num_rep == 100:
                    add_correct_solution(all_s, correct_s, correct_solutions_100)
                elif num_rep == 200:
                    add_correct_solution(all_s, correct_s, correct_solutions_200)
        storage.store_object(correct_solutions_10, "correct_solutions_10.csv", type="LIST")
        storage.store_object(correct_solutions_100, "correct_solutions_100.csv", type="LIST")
        storage.store_object(correct_solutions_200, "correct_solutions_200.csv", type="LIST")
    else:
        correct_solutions_10 = list(map(float, storage.get_object("correct_solutions_10.csv", type="LIST")[0]))
        correct_solutions_100 = list(map(float, storage.get_object("correct_solutions_100.csv", type="LIST")[0]))
        correct_solutions_200 = list(map(float, storage.get_object("correct_solutions_200.csv", type="LIST")[0]))
    plt.plot(sizes, correct_solutions_10, label="num_repeat=10")
    plt.plot(sizes, correct_solutions_100, label="num_repeat=100")
    plt.plot(sizes, correct_solutions_200, label="num_repeat=200")
    plt.xticks(sizes)
    plt.ylabel('correct solution [%]')
    plt.xlabel('size')
    plt.legend()
    plt.savefig("./figures/accuracity_vs_size.png")
