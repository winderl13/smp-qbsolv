import statistics

from algorithms import storage
import matplotlib.pyplot as plt


def main():
    """
    duration solve figure
    sets:   sizes = [5, 8, 11, 14, 17, 20]
            p1s = [0.1, 0.5, 0.9]
            p2s = [0.1, 0.5, 0.9]
            among all 50 instances
    :return:
    """
    plt.clf()
    sizes = [5, 8, 11, 14, 17, 20]
    p1s = [0.1, 0.5, 0.9]
    p2s = [0.1, 0.5, 0.9]
    mean_qbsolv = []
    mean_lp = []
    mean_shiftbrk = []
    mean_kirialy = []
    for size in sizes:
        mean_qbsolv_1 = []
        mean_lp_1 = []
        mean_shiftbrk_1 = []
        mean_kirialy_1 = []
        for p1 in p1s:
            for p2 in p2s:
                res = storage.get_smti_computational_time(size, p1, p2)
                res = list(map(lambda x: [x[i] for i in range(len(x)) if i % 3 == 2], res[1:]))
                res_0 = list(map(lambda x: round(1000 * float(x[0]), 4), res))  # convert to ms
                res_1 = list(map(lambda x: round(1000 * float(x[1]), 4), res))
                res_2 = list(map(lambda x: round(1000 * float(x[2]), 4), res))
                res_3 = list(map(lambda x: round(1000 * float(x[3]), 4), res))
                mean_qbsolv_1.append(statistics.mean(res_0))
                mean_lp_1.append(statistics.mean(res_1))
                mean_shiftbrk_1.append(statistics.mean(res_2))
                mean_kirialy_1.append(statistics.mean(res_3))
        mean_qbsolv.append(statistics.mean(mean_qbsolv_1))
        mean_lp.append(statistics.mean(mean_lp_1))
        mean_shiftbrk.append(statistics.mean(mean_shiftbrk_1))
        mean_kirialy.append(statistics.mean(mean_kirialy_1))
    plt.plot(sizes, mean_qbsolv, label="QUBO-MAX-SMTI")
    plt.plot(sizes, mean_lp, label="MAX-SMTI-LP")
    plt.plot(sizes, mean_shiftbrk, label="SHIFTBRK")
    plt.plot(sizes, mean_kirialy, label="Krialy2")
    plt.xticks(sizes)
    plt.ylabel('time [ms]')
    plt.xlabel('problem size')
    plt.legend()
    plt.savefig("./figures/duration_solve.png")
