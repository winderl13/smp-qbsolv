##############################
# general instance data
##############################

p1s = [0.1, 0.5, 0.9]
p2s = [0.1, 0.5, 0.9]
num = 50
algorithms = ["qubo", "lp", "shiftbrk", "kiraly"]

###############################
# qubo data/params
###############################


num_reps = [10, 100, 200]
tests_per_qubo = 6
