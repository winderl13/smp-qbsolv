import logging
import multiprocessing
from copy import deepcopy

import numpy as np

from algorithms import storage
from algorithms.solver.SMTI.lp_smti import LP_smti
from algorithms.solver.SMTI.qubo_smti import QbsolvSMTI
from algorithms.storage import get_smti
from computations.config import *
import algorithms.utils as ut

log = logging.getLogger()


def main():
    ut.init_log()
    p = multiprocessing.Pool(multiprocessing.cpu_count())
    tasks = []
    sizes = [5, 10, 15, 20, 25, 30]  # for accuracity
    for size in sizes:
        for p1 in p1s:
            for p2 in p2s:
                log.info(f"Creating Tasks: {size}, {p1}, {p2}")
                for index_f in range(num):
                    tasks.append((size, p1, p2, num, index_f,))
    results = [p.apply_async(sizes_num_rep, tasks) for tasks in tasks]
    for res in results:
        res.get()


def sizes_num_rep(size, p1, p2, num, index_f):
    """
    compute for a matching (described by: size, p1, p2, num and index_f qbsolv 10 times,
    also compute once the lp version and encode that solution on the qubo vector, the result will be stored too
    :param size:
    :param p1:
    :param p2:
    :param num:
    :param index_f:
    :return:
    """
    log.info(f"At Task: {size}, {index_f}, {p1}, {p2}")
    matching = get_smti(size, num, p1, p2, index_f)
    lp_res = compute_lp_energy(matching)
    for num_repeat in num_reps:
        for retry in range(tests_per_qubo):
            log.info(f"At Task: {size}, {p1}, {p2}: num_rep: {num_repeat}, retry: {retry}")
            solver = QbsolvSMTI(matching)
            response = solver.solve(num_repeats=num_repeat, debug=True)
            storage.store_response(lp_res, response, solver, size, num, p1, p2, index_f, num_repeat, retry)


def compute_lp_energy(matching):
    solver = QbsolvSMTI(matching, mode="np").create_qubo()
    solution_lp = LP_smti(matching).solve()
    x = ut.compute_qubo_vector_lp(solution_lp, solver)
    x_T = deepcopy(x)
    np.transpose(x_T)
    xQ = np.matmul(x_T, solver.qubo)
    return np.matmul(xQ, x), solution_lp.is_stable()
