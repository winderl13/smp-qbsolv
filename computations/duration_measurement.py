import logging
import multiprocessing
import timeit
import algorithms.utils as ut
from algorithms import storage

from algorithms.solver.SMTI.kiraly.kiralySMTI import Kirialy2
from algorithms.solver.SMTI.lp_smti import LP_smti
from algorithms.solver.SMTI.qubo_smti import QbsolvSMTI
from algorithms.solver.SMTI.shift_brk.shift_brk import ShiftBrk
from algorithms.storage import get_smti
from computations.config import *

log = logging.getLogger()


def create_setup(size, p1, p2, index_f, num):
    return f"""
from algorithms.solver.SMTI.kiraly.kiralySMTI import Kirialy2
from algorithms.solver.SMTI.lp_smti import LP_smti
from algorithms.solver.SMTI.qubo_smti import QbsolvSMTI
from algorithms.solver.SMTI.shift_brk.shift_brk import ShiftBrk
from algorithms.storage import get_smti
matching = get_smti({size}, {num}, {p1}, {p2}, {index_f})
    """


def get_solver(solver):
    return f"""
{solver}(matching).solve()
    """


def solve_measurement(solver, setup, t_r=10):
    return timeit.timeit(get_solver(solver), setup=setup, number=t_r) / float(t_r)


def main():
    ut.init_log()
    p = multiprocessing.Pool(multiprocessing.cpu_count())
    sizes = [5, 8, 11, 14, 17, 20]  # for duration
    tasks = []
    for size in sizes:
        for p1 in p1s:
            for p2 in p2s:
                create_header(size, num, p1, p2)
                for index_f in range(num):
                    tasks.append((size, num, p1, p2, index_f,))
    results = [p.apply_async(compute_duration_measurement, tasks) for tasks in tasks]
    for res in results:
        res.get()


def create_header(size, num, p1, p2):
    all_res = []
    for algorithm in algorithms:
        all_res.append(f"{algorithm}_stable")
        all_res.append(f"{algorithm}_size")
        all_res.append(f"{algorithm}_dt[s]")

    storage.create_results(all_res, f"/size_{size}_num_{num}", f"p1_{p1}p2_{p2}.csv")


def compute_duration_measurement(size, num, p1, p2, index_f):
    matching = get_smti(size, num, p1, p2, index_f)
    setup = create_setup(size, p1, p2, index_f, num)
    all_res = []
    for algorithm in algorithms:
        log.info(f"at {size} {p1} {p2} {index_f}: {algorithm}")
        res = eval_algorithm(matching, algorithm).is_stable()
        d_t = solve_measurement(get_algorithm_name(algorithm), setup)
        all_res.append(res[0])
        all_res.append(res[1])
        all_res.append(d_t)
    storage.store_results(all_res, f"/size_{size}_num_{num}", f"p1_{p1}p2_{p2}.csv")


def get_algorithm_name(algorithm):
    if algorithm == "qubo":
        return "QbsolvSMTI"
    elif algorithm == "lp":
        return "LP_smti"
    elif algorithm == "shiftbrk":
        return "ShiftBrk"
    elif algorithm == "kiraly":
        return "Kirialy2"
    else:
        raise Exception(f"unknown algorithm: {algorithm}")


def eval_algorithm(matching, algorithm):
    # matching = get_smti(size, num, p1, p2, index_f)
    if algorithm == "qubo":
        return QbsolvSMTI(matching).solve()
    elif algorithm == "lp":
        return LP_smti(matching).solve()
    elif algorithm == "shiftbrk":
        return ShiftBrk(matching).solve()
    elif algorithm == "kiraly":
        return Kirialy2(matching).solve()
    else:
        raise Exception(f"unknown algorithm: {algorithm}")
