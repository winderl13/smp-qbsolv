from algorithms.create_templates import create_and_save_smti, compute_and_store_header

from computations.config import *


def main():
    # sizes = [5, 10, 15, 20, 25, 30]  # for accuracity
    sizes = [i for i in range(3, 31, 1)]
    for size in sizes:
        for p1 in p1s:
            for p2 in p2s:
                for index_f in range(num):
                    matching = create_and_save_smti(num, index_f, size, p1, p2)
                    compute_and_store_header(matching, num, p1, p2, index_f)
